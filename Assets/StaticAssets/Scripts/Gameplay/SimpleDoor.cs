﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleDoor : NetworkBehaviour
{
    public Vector3 openPosition;
    public Vector3 closedPosition;

    [SyncVar]
    public bool isOpen = false;

    public List<AudioClip> openSounds;
    public List<AudioClip> closeSounds;

    [Tooltip("Door open/close speed")]
    public float speed = 1;

    public void Update()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, isOpen ? openPosition : closedPosition, Time.deltaTime * speed);
    }

    public void Toggle()
    {
        isOpen = !isOpen;
        PlayAudio();
    }

    private void PlayAudio()
    {
        AudioClip rndClip = isOpen ? (openSounds.Count > 0 ? openSounds[UnityEngine.Random.Range(0, openSounds.Count)] : null) : (closeSounds.Count > 0 ? closeSounds[UnityEngine.Random.Range(0, closeSounds.Count)] : null);
        if (rndClip != null)
            SoundManager.Instance.PlaySound(rndClip, 1, false, transform);
    }
}