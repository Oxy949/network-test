﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkIdentity))]
[RequireComponent(typeof(NetworkTransform))]
public class NetworkPlayerController : NetworkBehaviour
{
    [Tooltip("Max distance between obj and player")]
    public float activateDistance = 1;

    public string interactableObjectTag = "InteractableObject";

    public GameObject interactCanvas;
    
    void Start()
    {
        //Disable camera for other players
        if (!isLocalPlayer)
        {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            interactCanvas.SetActive(false);
        }
    }
    
    void Update()
    {
        interactCanvas.SetActive(false);

        if (!isLocalPlayer) //Stop update for others
            return;
        
        RaycastHit hit;
        Ray ray = GetComponentInChildren<Camera>().ScreenPointToRay(Input.mousePosition);


        if (Physics.Raycast(ray, out hit, activateDistance))
        {
            if (hit.collider != null && hit.collider.tag == interactableObjectTag) // If raycast hit a button
            {
                interactCanvas.SetActive(true);

                if (Input.GetButtonDown("Interact"))
                    CmdInteractWith(hit.collider.gameObject); // Send cmd to server
            }
        }
    }

    [Command]
    public void CmdInteractWith(GameObject obj) // Only on server
    {
        obj.GetComponent<NetworkButton>().OnClick();
        obj.GetComponent<NetworkButton>().RpcOnClick();
    }
}