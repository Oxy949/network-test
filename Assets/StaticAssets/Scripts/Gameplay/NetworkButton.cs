﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class NetworkButton : NetworkBehaviour
{
    public UnityEvent onClickEvents;
    
    public void OnClick()
    {
        if (isServer && !isLocalPlayer)
            onClickEvents.Invoke();
    }

    [ClientRpc]
    public void RpcOnClick()
    {
        onClickEvents.Invoke();
    }
}
